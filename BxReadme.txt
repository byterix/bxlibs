======================================
======== Библиотека iByteriX =========
======================================
ByteriX, 2013-2015. All right reserved.

Версия 0.2.31 betta (18.02.2016)
- enable bitcode flags

Версия 0.2.30 (08.02.2016)
- добавил клик в BxViewItemsList

Версия 0.2.29 (24.12.2015)
- добавил категорию для sha1 из строки

Версия 0.2.28 (09.12.2015)
- убрал базовые warnings

Версия 0.2.27 (04.12.2015)
- поддержка BxDownloadStreamSaver.request
- fix isues

Версия 0.2.26 (29.11.2015)
- Добавлена очередь обработки пушнотификаций BxPushNotificationMessageQueue
- поправлено падение при регистрации deviceToken

Версия 0.2.25 (21.11.2015)
- Начало переписывания библиотеки под Swift совместимость
- BxServiceDataCommand nonnull /nullable

Версия 0.2.24 (20.11.2015)
- расширены возможности закачки при ошибке в BxDownloadStream
- добавлен метод checkResult в BxServiceDataSet
- ошибки через [BxAbstractDataCommand errorWithCode:message:]
- парсинг данных при ошибках в загрузке BxServiceDataCommand

Версия 0.2.23 (29.10.2015)
- BxOldInputTableController поправлена логика работы с клавиатурой
- добавлено описание и исправлено обновление клавиатуры

Версия 0.2.22 (27.10.2015)
- попправил отображение blur в iPhone 6(+)
- добавил немного описания

Версия 0.2.21 (17.10.2015)
- убрал заглушку загрузки данных в GET методе

Версия 0.2.20 (14.09.2015)
- правильная отрисовка интерфейса BxLoadedImageListViewController

Версия 0.2.19 (22.08.2015)
- BxTextView - добавлен компонент
- исправлен баг с отрисовкой BxTextView

Версия 0.2.18 (20.08.2015)
- BxOldInputTableController - поправил баг, связанный с секьюрными полями

Версия 0.2.17 (08.08.2015)
- BxOldInputTableController - поправил баг, связанный с опознованием открытой клавиатуры для contentRect

Версия 0.2.16 (25.06.2015)
- BxViewItemsList - поправил баг, обновил тестовое приложение

Версия 0.2.15 (19.06.2015)
- BxNavigationController поправил для того чтобы пряталась панель при повторном check

Версия 0.2.14 (11.03.2015)
- Переход с JSONKit на NSJSONSerialization

Версия 0.2.13 (23.01.2015)
- Поддержка ios7 нарезки для iOS8 устройств нового поколения
- Настройка проекта для сборки в AppCode 3.1

Версия 0.2.12 (12.01.2015)
- Добавлена категория NSURL+BxUtils с методами работы с атрибутами восстановления из storage
- Возможность восстановления из storage бд, по умолчанию отключена для BxDataBase
- Поддержка x86_64 архитектуры

Версия 0.2.11 (26.12.2014)
- BxOldInputTableController: пробелы при вводе теперь отображаются
- BxOldInputTableController: корректные переходы при нажатии на Назад-Вперед на панели клавиатуры
- BxOldInputTableController: адаптация под iPhone 6 (+) по ширине ячейки
- BxSplashViewController: повторный показ теперь возможен

Версия 0.2.10 (25.12.2014)
- Починил  -568h, -667h, -736h для @2x, @3x

Версия 0.2.9 (16.12.2014)
- BxDataBase можно указать путь, а не только название файла

Версия 0.2.8 (10.12.2014)
- Перекройка BxSplashViewController для iOS 8 и новых девайсов
- Поддержка iPhone 6 (+) для UIImage, интелектуальный поиск нарезки с префиксами -568h, -667h, -736h, -ios7

Версия 0.2.7 (09.12.2014)
- BxNavigationController совместимость с iOS 6
- адаптировали BxLocationManager к iOS 8

Версия 0.2.6 (02.12.2014)
- BxNavigationController устранил утечки в контролерах

Версия 0.2.5 (20.11.2014)
- BxNavigationController уладили работу со свайпом из угла
- BxNavigationController добавлена возможность работы с загруженным представлением в контролере
- BxNavigationController поправил вызов выхода при свайпе

Версия 0.2.4 (24.10.2014)
- почин BxPageControl с кастомными точками
- верное расположение BxPageControl на BxIconWorkspaceView

Версия 0.2.3 (14.10.2014)
- Переработка макросов для RETINA
- BxIconWorkspaceItemControl - передача изображения через UIImage
- BxIconWorkspaceView - изменили логику формирования страниц
- BxOldInputTableController поправил косяк со шрифтами
- BxOldInputTableController поправил косяк с сохранением значения

Версия 0.2.2 (09.10.2014)
- В BxIconWorkspaceView добавлено свойство editabled
- BxIconWorkspaceView подправлено нажатие на кнопку
- IS_480, IS_667, IS_736 для поддержки iPhone 6, iPhone 6+

Версия 0.2.1 (02.10.2014)
- поддержка iOS 8
- blur эффект
- autoBackground модального контролера BxServiceController

Версия 0.2 (01.10.2014)
- переделка программного интерфейса BxViewItemsList
- добавление вертикального пролистывания в BxViewItemsList
- добавление индикатора в BxViewItemsList
- добавление многомерной таблицы BxTreeListScrollView
- добавление рабочего стола BxIconWorkspaceView

Версия 0.1.19 (27.08.2014)
- добавлен BxXmlDataParser

Версия 0.1.18 (28.07.2014)
- BxNavigationController поддержка "batch pop to controllers"

Версия 0.1.17 (21.07.2014)
- интерфейс доступа BxNavigationController + animated

Версия 0.1.16 (21.07.2014)
- добавлен BxNavigationBar + toolPanel для навигационной панели в BxNavigationController
- поддержка ios 6 для BxNavigationBar + более четкий поиск фоновой панели
- исправлена ошибка загрузки первого контролера для BxNavigationController

Версия 0.1.15 (06.06.2014)
- Поправлен parser в BxServiceDataSet
- В BxAbstractDataCommand добавлен метод для работы с блоками

Версия 0.1.14 (12.05.2014)
- Убрал все лишние костыли в BxWebDocViewController

Версия 0.1.13 (04.05.2014)
- добавил BxFileDataSet набор данных из файла
- сделал ссылки библиотек друг на друга

Версия 0.1.12 (16.04.2014)
- добавили поле "Не выбран" в барабан BxOldInputTableController
- сделали возможность выбора элемента "Не выбран"

Версия 0.1.11 (10.04.2014)
- отступы наладили

Версия 0.1.10 (09.04.2014)
- рефакторинг работы ячейки BxOldInputTableController
- ячейки создаются всего один раз BxOldInputTableController
- дополнительная кастомизация по длине разделителя для iOS7
- возможность кастомизации отступов
- поправлена бага стирания поля

Версия 0.1.9 (08.04.2014)
- рефакторинг работы ячейки BxOldInputTableController
- возможность расширения по высоте ячейки BxOldInputTableController
- правка и устранение рудиментов BxOldInputTableController
- поправлены ошибки расположения поля ввода BxOldInputTableController

Версия 0.1.8 (27.03.2014)
- переход на ячейки заполнения BxOldInputTableController

Версия 0.1.7 (25.03.2014)
- обновление даты в BxOldInputTableController
- поправлен баг в getEnabledFromFieldName

Версия 0.1.6 (19.03.2014)
- Сделал возможность шерокоформатного выбора галочек в BxOldInputTableController
- Для iOS7 настроил отступ у окна ввода BxOldInputTableController
- Возможность добавления header и fother в виде UIView для BxOldInputTableController

Версия 0.1.5 (13.02.2014)
- Сделал пример листания документов

Версия 0.1.4 (06.02.2014)
- Поправил баг с вылазаньем клавиатуры BxOldInputTableController

Версия 0.1.3 (30.01.2014)
- Навел порядок в отступах табличек BxOldInputTableController

Версия 0.1.2 (30.01.2014)
- Расширен BxNavigationController::immediatePopViewControllerAnimated:

Версия 0.1.1 (30.01.2014)
- Свой BxNavigationController
- Отмена выхода из дочернего контролера относительно навигационного

Версия 0.1 (28.01.2014)
- Создал репозиторий для скомпилированной библиотеки