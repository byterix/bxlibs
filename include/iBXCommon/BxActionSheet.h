//
//  BxActionSheet.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 7/4/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BxActionSheetHandler)(int buttonIndex);

@interface BxActionSheet : UIActionSheet <UIActionSheetDelegate>

//! стандартный ActionSheet через блоки
+ (void) showActionSheetWithTitle: (NSString *) title
                cancelButtonTitle: (NSString *) cancelButtonTitle
                otherButtonTitles: (NSArray *) otherButtonTitles
                             view: (UIView*) view
                          handler: (BxActionSheetHandler) handler;

@end
