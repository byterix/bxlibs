//
//  BxAlertView.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 7/4/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BxAlertHandler)(BOOL isOK);

@interface BxAlertView : UIAlertView

//! Стандартное диалоговое окно через блоки
+ (void) showAlertWithTitle: (NSString *) title
                    message: (NSString *) message
          cancelButtonTitle: (NSString *) cancelButtonTitle
              okButtonTitle: (NSString *) okButtonTitle
                    handler: (BxAlertHandler) handler;

//! Отображение ошибки программы
+ (void) showError: (NSString *) message;

//! Отображает ошибку и завершает выполнение программы после нажатия на ОК
+ (void) showErrorAndExit: (NSString *) message;

@end
