//
//  BxUtils.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 7/4/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BxUtils : NSObject

//! возращает название количества count из набора названий words для русскоязычной локали
+ (NSString *) getWordFromCount: (int) count with3Words: (NSArray*) words;

//! Возвращает случайное число от 0 до 1.0
+ (double) getRandom;

@end
