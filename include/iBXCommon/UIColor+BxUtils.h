//
//  UIColor+BxUtils.h
//  iBXCommon
//
//  Created by Sergan on 30.08.13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BxUtils)

+ (id) colorWithHex: (UInt32) rgbValue alpha: (float) alpha;
+ (id) colorWithHex: (UInt32) rgbValue;
+ (id) colorFromHexString: (NSString*) hexString  alpha: (float) alpha;
+ (id) colorFromHexString: (NSString*) hexString;

@end
