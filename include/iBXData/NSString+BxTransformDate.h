//
//  NSString+BxTransform.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/9/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef long long JPlusDateTime;

@interface NSString (BxTransformDate)

//! Преобразование серверной строковой даты в объект
- (NSDate*)dateFromRfc3999Value;

+ (id) stringWithTime: (JPlusDateTime) time;
+ (id) stringWithJTime: (NSDate*) date;
- (JPlusDateTime) getJPlusDateTime;

@end
