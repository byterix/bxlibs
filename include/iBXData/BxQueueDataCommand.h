//
//  BxQueueDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxGroupDataCommand.h"

//! Очередь исполнения команд
@interface BxQueueDataCommand : BxGroupDataCommand {
@protected
	NSMutableDictionary * registrationCommands;
	NSCondition * condition;
	NSLock * lockCommands;
	BOOL isExecuted;
    BOOL isPause;
}

- (BxAbstractDataCommand*) objectForData: (NSDictionary*) data;

- (void) update;

- (void) registryCommandClass: (Class) dataCommandClass;

- (void) start;

- (void) pause;

- (void) resume;

/**
 *	Запускает очередь с ожиданием исполнения,
 *	поскольку может останавливать основной поток,
 *	вызов данного метода из основного потока приведет к deadlock
 *	Смысла вызывать его никакого нет, проще execute у команды
 */
- (void) startAndWait;

- (void) removeCommand: (BxAbstractDataCommand*) command;

@end

@interface BxQueueDataCommand (private)

- (void) errorFromCommand: (BxAbstractDataCommand *) command exception: (NSException *) exception;

@end