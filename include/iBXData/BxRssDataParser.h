//
//  BxRssDataParser.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/9/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataParser.h"

@interface BxRssDataParser : BxAbstractDataParser <NSXMLParserDelegate> {
    NSMutableDictionary * _item;
    NSString * _url;
}

- (void) addContent: (NSString *)string forTag: (NSString*) localtagName;

@end
