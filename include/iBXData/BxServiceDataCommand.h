//
//  BxServiceDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataCommand.h"
#import "BxDownloadStream.h"
#import "BxAbstractDataParser.h"

typedef enum
{
    BxServiceMethodGET,
    BxServiceMethodPOST,
    BxServiceMethodPUT,
    BxServiceMethodDELETE
} BxServiceMethod;

//! Команда, выполняющая действия на серевере, через веб-сервис
@interface BxServiceDataCommand : BxAbstractDataCommand{
@protected
    NSString * url;
	NSString * caption;
    NSString * requestBody;
	NSDictionary * result;
	NSDictionary * rawResult;
	id data;
    BxServiceMethod method;
    BxDownloadStream * stream;
}
@property (nonatomic, readonly, nonnull) NSString * url;
@property (nonatomic, readonly, nullable) NSDictionary * result;
@property (nonatomic, readonly, nullable) NSDictionary * rawResult;
@property (nonatomic, readonly, nullable) id data;
@property (nonatomic, retain, nonnull) BxAbstractDataParser * parser;

- (nonnull instancetype) initWithUrl: (nonnull NSString*) url
              data: (nullable id) data
            method: (BxServiceMethod) method
           caption: (nullable NSString*) caption1;

//! Если объект data1 пустой, то вызовется GET, иначе POST
- (nonnull instancetype) initWithUrl: (nonnull NSString*) url
              data: (nullable id) data1
           caption: (nullable NSString*) caption1;

//! @private
- (void) updateRequest: (nonnull NSMutableURLRequest*) request;
//! @private
- (nullable NSString *) getRequestBody;

- (nullable NSDictionary*) checkResult: (nullable NSDictionary*) dataResult;



@end