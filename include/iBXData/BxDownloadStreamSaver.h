//
//  BxDownloadStreamSaver.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/23/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxDownloadProgress.h"

@interface BxDownloadStreamSaver : NSObject

@property (nonatomic, retain) NSMutableURLRequest * request;

/**
 *	@brief Сохранение всего содержимого html страницы со вложанными ссылками
 *
 *	Происходит сохранение синхронно, все события по управлению обрабатываются тут
 *	@param url [in] - ссылка на ресурс (например html страницу)
 *	@param path [in] - путь, куда ресурс будет сохранен в виде набора файлов с заглавной страницей
 *	@return Путь к заглавной странице ресурса
 *	@throw DownloadException
 */
- (NSString*) saveFrom: (NSString*)url
					to: (NSString*)path
			  delegate: (id<BxDownloadProgress>) delegate;

- (NSString*) saveFrom: (NSString*)url
					to: (NSString*)path
			  delegate: (id<BxDownloadProgress>) delegate
          isContaining: (BOOL) isContaining
       containingLevel: (int) containingLevel;;

- (NSString*) saveFrom: (NSString*)url
					to: (NSString*)path
			  delegate: (id<BxDownloadProgress>) delegate
				   ext: (NSString *) ext;

- (NSString*) saveFrom: (NSString*)url
					to: (NSString*)path
			  delegate: (id<BxDownloadProgress>) delegate
				   ext: (NSString *) ext
          isContaining: (BOOL) isContaining
       containingLevel: (int) containingLevel;

@end
