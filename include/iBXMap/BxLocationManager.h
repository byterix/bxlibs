//
//  BxLocationManager.h
//  iBXMap
//
//  Created by Balalaev Sergey on 6/11/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

typedef void (^BxOneLocationHandler) (CLLocation *location, NSString * messageError);

@interface BxLocationManager : NSObject <CLLocationManagerDelegate>
{
@protected
    CLLocationManager * manager;
    BOOL isOneCheck;
    BOOL isStarted;
    BOOL isStartedOne;
}

+ (BxLocationManager*) default;

//! Generate messageError, if location not found in this time. Default 30 sec
@property NSTimeInterval timeout;

//! For getLocationBlock the max time at update location. Default 0.2 sec
@property NSTimeInterval maxRefreshTime;

// Method for displaying authorization dialogue in iOS8+ (called authomatically during singletone initaialization)
// Don't forget to include in your app plist:

//	<key>NSLocationAlwaysUsageDescription</key>
//  <string></string>
//  <key>NSLocationWhenInUseUsageDescription</key>
//  <string></string>

- (void) requestAuthorization;

//! only first detected location return
- (void) getOneLocationBlock: (BxOneLocationHandler) locationFound;

//! detected location with refreshing, limited maxRefreshTime
- (void) getLocationBlock: (BxOneLocationHandler) locationFound;

- (void) cancel;

@end
