//
//  BxDBDataSet.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/8/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxDatabase.h"

@interface BxDBDataSet : NSObject
{
@protected
    BOOL isLoaded;
    int count;
    BxDatabase * dataBase;
    int bufferCount;
    NSMutableArray * bufferArray;
    int bufferOffset;
}
- (id) initWithDB: (BxDatabase*) db countSQL: (NSString*) countSQL itemsSQL: (NSString*) itemsSQL;
- (id) initWithDB: (BxDatabase*) db tableName: (NSString*) tableName orders: (NSString*) orders;
- (id) initWithTableName: (NSString*) tableName orders: (NSString*) orders;
- (id) initWithCountSQL: (NSString*) countSQL itemsSQL: (NSString*) itemsSQL;

- (void) update;

- (int) count;

- (id) dataFromIndex: (int) index;

- (NSArray*) allData;

//! Эти методы некорректны и никакого отношения не имеют к наборам данных, они перенесены в BxDatabase
+ (BOOL) executeFromDB: (BxDatabase*) database sql: (NSString*) sql NS_DEPRECATED(10_0, 10_8, 2_0, 7_0);
+ (BOOL) executeWith: (NSString*) sql NS_DEPRECATED(10_0, 10_8, 2_0, 7_0);
+ (NSNumber*) executeNumberFunctionWith: (NSString*) sql NS_DEPRECATED(10_0, 10_8, 2_0, 7_0);
+ (NSArray*) allDataWith: (NSString*) sql NS_DEPRECATED(10_0, 10_8, 2_0, 7_0);

@end