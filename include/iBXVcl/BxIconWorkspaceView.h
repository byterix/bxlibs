//
// Created by Balalaev Sergey on 9/29/14.
// Copyright (c) 2014 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BxIconWorkspacePageView.h"
#import "BxIconWorkspaceItemControl.h"

@class BxPageControl;
@class BxIconWorkspaceView;

@protocol BxIconWorkspaceDelegate <NSObject>

- (void) iconWorkspaceView: (BxIconWorkspaceView*) view onEdition: (BOOL) onEdition;


@end


@interface BxIconWorkspaceView : UIView <UIScrollViewDelegate>{
@protected
    BxPageControl *_pageControl;
    NSArray *_pages;
    BxIconWorkspacePageView *_currentPage;
    UIScrollView *_scrollView;
    NSTimer *_updateMovedTimer;
    NSString *_currentFileName;
    int _maxRowCount;
    int _maxColCount;
    BOOL _isChangePage;
    BOOL _pageControlUsed;
}

@property (nonatomic, readonly) BxPageControl *pageControl;
@property (nonatomic, readonly) NSArray *pages;
@property (nonatomic, readonly) BxIconWorkspacePageView *currentPage;
@property (nonatomic, readonly) UIScrollView *scrollView;

@property (nonatomic) BOOL editable;

@property  (nonatomic, weak) id<BxIconWorkspaceDelegate> delegate;

//! завершение редактирования с сохранением
- (void) done;

/**
 *    заготовленные странички BxIconWorkspacePageView with BxIconWorkspaceItemControl
 *    idName необходим для сохранения состояния иконок
  */
- (void) updateWithPages: (NSArray *) pages idName: (NSString*) idName;

//! размер иконки
@property (nonatomic) CGSize iconSize UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indentIconHeight UI_APPEARANCE_SELECTOR;
@property (nonatomic, readonly) BOOL pageControlUsed;

- (void) updateIcons;

- (NSUInteger) currentPageIndex;

@end