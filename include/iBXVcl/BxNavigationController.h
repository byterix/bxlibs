//
//  BxNavigationController.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 1/30/14.
//  Copyright (c) 2014 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BxNavigationController;

@interface UIViewController (BxNavigationController)

//! Если возвращает NO, то не позволяет покинуть контроллер, например для завершения операции. По умолчанию YES
- (BOOL) navigationShouldPopController: (BxNavigationController*) navigationController;
//! Этот контролер вызывается в случае pop батчем, правильнее его использовать совместно с функцией immediatePopViewController: animated:, однако он совершенно не покрывает navigationShouldPopController:
- (BOOL) navigationShouldPopController: (BxNavigationController*) navigationController toController: (UIViewController*) toController;

//! Перед реальным выходом вызывается
- (void) navigationWillPopController: (BxNavigationController*) navigationController;

//! Если определен как не nil, то эта понель по высоте будет показана ниже navigationBar. Высота панельки влияет на extendedEdgesBounds, учитывайте этот факт. Оптимальней будет, если вы это представление закешируете в своем контролере. Метод может вызываться до создания представления контролера.
- (UIView*) navigationToolPanelWithController: (BxNavigationController*) navigationController;

@end

@interface BxNavigationController : UINavigationController

@property (nonatomic, weak, readonly) UIView * toolPanel;

//! реальный, немедленный выход с контролера.
- (UIViewController *) immediatePopViewControllerAnimated:(BOOL)animated;
- (NSArray *) immediatePopViewController: (UIViewController *) toController animated:(BOOL)animated;

//! Производит проверку контролера на отображение панели сверху
- (void)checkPanelController:(UIViewController *)viewController animated: (BOOL) animated;

//! Прячет верхнюю панель
- (void) hidePanelAnimated: (BOOL) animated;

@end
