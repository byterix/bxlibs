//
// Created by Balalaev Sergey on 9/29/14.
// Copyright (c) 2014 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BxIconWorkspaceItemControl;


@interface BxIconWorkspacePageView : UIView
{
}

@property (nonatomic, strong, readonly) NSMutableArray * icons;

- (void) addIcon: (BxIconWorkspaceItemControl*) icon;
- (void) insertIcon: (BxIconWorkspaceItemControl*) icon atIndex: (NSUInteger) index;

@end