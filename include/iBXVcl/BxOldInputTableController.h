//
//  InputTableController.h
//

#import <Foundation/Foundation.h>
#import "BxKeyboardController.h"
#import "BxOldInputTableCell.h"

extern const NSString * const FNInputTableHeader; // может быть строкой и UIView
extern const NSString * const FNInputTableFooter; // может быть строкой и UIView
extern const NSString * const FNInputTableRows;
extern const NSString * const FNInputTableRowHint;
extern const NSString * const FNInputTableRowValue;
extern const NSString * const FNInputTableRowFieldName;
extern const NSString * const FNInputTableRowIsSecurity;
extern const NSString * const FNInputTableRowIsAction;
extern const NSString * const FNInputTableRowIsEnabled;
extern const NSString * const FNInputTableRowIsSwitch;

extern const NSString * const FNInputTableRowVariants;
extern const NSString * const FNInputTableRowVariantNullTitle; // название не выбранного элемента, по умолчанию ''
extern const NSString * const FNInputTableRowVariantIsNullSelected; // позволять выбрать пустое значение, по умолчанию отключено
extern const NSString * const FNInputTableRowIsDatePicker;
extern const NSString * const FNInputTableRowSubtitle;
extern const NSString * const FNInputTableRowPlaceholder;
extern const NSString * const FNInputTableRowMaxSymbolsCount;
extern const NSString * const FNInputTableRowMaxChecksCount; // по умолчанию 0
extern const NSString * const FNInputTableRowIcon;
extern const NSString * const FNInputTableRowKeyboardType;

@interface BxOldInputTableController : BxKeyboardController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,  UIAppearance> {
@protected
	UIImageView * imageView;
	UIScrollView * mainScroll;
	UITableView * mainTable;
	UIView * textInputView;
	UITextField * textInput;
	CGRect textInputFrame;
	NSArray * info;
	UIButton * btSend;
    UIPickerView * variantPicker;
    NSArray * variants;
    NSString * currentFieldName;
    UIDatePicker * datePicker;
    
    NSIndexPath * indexPathForSelectedCell;
    NSMutableDictionary * currentElement;
    CGRect _contentRect;
    BOOL _isContentChanged; // если этот параметр YES, то вероятно сработала клавиатура и contentRect отщепился от contentView, так как в этом котролере размер его не меняется в отличае от предка
}
@property (nonatomic, retain) NSArray * info;
@property (nonatomic, retain) NSMutableDictionary * currentElement;
@property (nonatomic, retain) NSDateFormatter * dateFormatter;

@property (nonatomic, retain) UIColor * titleTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor * subtitleTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor * valueTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor * placeholderColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIFont * titleFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIFont * subtitleFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIFont * valueFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIImage * backgroundImage UI_APPEARANCE_SELECTOR;
@property (nonatomic) BOOL isFloatCellSize UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat lineShift UI_APPEARANCE_SELECTOR;


- (void) startWithData: (NSDictionary*) data editing: (BOOL) editing;
- (id) getDataFromFieldName: (NSString*) fieldName;
- (id) getValueFromFieldName: (NSString*) fieldName;
- (void) setValue: (id) value fromFieldName: (NSString*) fieldName;
- (BOOL) getEnabledFromFieldName: (NSString*) fieldName;
- (void) setEnabled: (BOOL) value fromFieldName: (NSString*) fieldName;

+ (UIFont *) getTitleFont;
+ (UIFont *) getSubtitleFont;
+ (UIFont *) getValueFont;

//! возвращает данные для ячейки по индексу
- (NSDictionary*) getRowDataFrom: (NSIndexPath*) indexPath;
- (NSMutableDictionary*) getRowDataFieldName: (NSString*) fieldName;

- (void) scrollDidLoad;

- (void) update;
- (void) updateValues;

// в этих методах можно определить стиль меток на текстовых полях таблицы
- (void) updateLabelForSectionHeader: (UILabel *) label;
- (void) updateLabelForSectionFooter: (UILabel *) label;

//! при смене поля можно изменить поведение клавиатуры
- (void) updateKeyboardTypeWithFieldName: (NSString*) fieldName;
//! после смены клавиатуры можно еще сто то сделать с полем ввода
- (void) updateTextInputFromName: (NSString*) fieldName;


// отступы задают
- (CGFloat) heightShift;
- (CGFloat) textShift;

// в процессе редактирования пользователь изменил значение поля
- (void) didChangedValue : (id) value fromFieldName: (NSString*) fieldName;

//! Выбор элемента для редактирования по названию поля
- (void) selectWithFieldName: (NSString*) fieldName;

//! Элемент является булевым
+ (BOOL) isBooleanRow: (NSDictionary*) row;

//! Для изменения отображаемой ячейки
- (BxOldInputTableCell *) cellFromData: (NSDictionary*) data;
- (Class) cellClass;
- (CGFloat) cellHeight;

@end
