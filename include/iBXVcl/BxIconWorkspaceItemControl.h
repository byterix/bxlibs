//
// Created by Balalaev Sergey on 9/29/14.
// Copyright (c) 2014 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BxIconWorkspaceView;

@interface BxIconWorkspaceItemControl : UIControl {
@protected
    UIImageView *_iconView;
    UILabel *_titleLabel;
    BxIconWorkspaceView *_owner;
    SEL _action;
    BOOL _isStart;
    BOOL _isStartMoved;
    double _angleShift;
}

@property (nonatomic, strong, readonly) UIImageView *iconView;
@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, weak) BxIconWorkspaceView * target;
@property (nonatomic) CGFloat indention UI_APPEARANCE_SELECTOR;


@property (nonatomic, copy) NSString * idName;

- (id)initWithTitle:(NSString *)title image:(UIImage *)image target:(id)target action:(SEL)action idName: (NSString*) idName1;

@end