//
//  HighlightedLabel.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BxHighlightedArea.h"

@interface BxHighlightedLabel : UIView {
@protected
	NSArray * areas;
	UIFont * font;
	NSString * text;
	UIColor * textColor;
	UIColor * markColor;
    UIColor * markTextColor;
	CGFloat height;
	UIColor * highlightedColor;
	UIFont * highlightedFont;
	BOOL isHighlightedTextFound;
	BOOL isLineWordwrap;
    
    NSTextAlignment textAlignment;
    CGFloat width;
    NSArray * highlightedLines;
    
    BOOL isSelected;
    UIColor * selectedTextColor;
    UIColor * markColorForAreaInFocus;
}

@property (nonatomic) CGFloat width;
@property (nonatomic, retain) NSArray * areas;
@property (nonatomic, retain) UIFont * font;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) UIColor * textColor;
@property (nonatomic, retain) UIColor * markColor;
@property (nonatomic, retain) UIColor * markTextColor;
@property (nonatomic, retain) UIColor * highlightedColor;
@property (nonatomic, retain) UIFont * highlightedFont;
@property (nonatomic, retain) NSArray * highlightedLines;
@property (nonatomic, retain) UIColor * markColorForAreaInFocus;

@property (nonatomic, retain) UIColor * selectedTextColor;
@property (nonatomic, assign) BOOL selected;
//! аналогичен selected с selectedTextColor
@property(nonatomic,getter=isHighlighted, setter = setHighlighted:) BOOL highlighted;

@property (nonatomic) NSTextAlignment textAlignment;

@property (nonatomic, readonly, getter = getHighlightRects) NSArray * highlightRects;

- (id) initWithWidth: (CGFloat) labelWidth 
			position: (CGPoint) position 
				font: (UIFont*) font
				text: (NSString*) text
       highlightText: (NSString*) highlightText 
		   markColor: (UIColor*) markColor;

- (id) initWithWidth: (CGFloat) labelWidth 
			position: (CGPoint) position 
				font: (UIFont*) font
				text: (NSString*) text 
			   areas: (NSArray*) areas 
		   markColor: (UIColor*) markColor;

/*- (id) initWithWidth: (CGFloat) labelWidth
			position: (CGPoint) position 
				font: (UIFont*) font1
				text: (NSString*) text1
       highlightText: (NSString*) highlightText 
		   markColor: (UIColor*) markColor1;*/

- (id) initWithWidth: (CGFloat) labelWidth 
			position: (CGPoint) position 
				font: (UIFont*) font1
				text: (NSString*) text1
       highlightText: (NSString*) highlightText 
		   markColor: (UIColor*) markColor1
       textAlignment: (NSTextAlignment) textAlignment1;

- (id) initWithWidth: (CGFloat) labelWidth 
			position: (CGPoint) position 
				font: (UIFont*) font;

- (void) showMarks: (CGContextRef) context;

- (NSArray*) preparedTextInfoWithWidth: (CGFloat) labelWidth;

- (NSArray*) getHighlightRectsWith: (HighlightType) type;

- (void) setText: (NSString*) text1 areas: (NSArray*) areas1;

- (void) setHighlightText: (NSString*) value;

- (void) update;

- (CGPoint) getStartWordPointFromPoint: (CGPoint) point indexPosition: (int*) indexPosition isWord: (BOOL) isWord;

- (CGPoint) getStopWordPointFromPoint: (CGPoint) point indexPosition: (int*) indexPosition  isWord: (BOOL) isWord;

/**
 *  Возвращает прямоугольную область канвы фразы, пересекаемую area
 *  Если в область входит больше одной строки то по ширине компонента результат.
 */
- (CGRect) rectFromArea: (BxHighlightedArea*) area;

@end
