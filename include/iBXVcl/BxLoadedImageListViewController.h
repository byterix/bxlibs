//
//  BxLoadedImageListViewController.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 10/9/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import "BxBaseViewController.h"
#import "BxPageScrollView.h"

@interface BxLoadedImageListItem : NSObject

- (id) initWithUrl: (NSString*) url title: (NSString*) title;

+ (id) itemWithUrl: (NSString*) url title: (NSString*) title;

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * title;

@end

@interface BxLoadedImageListViewController : BxBaseViewController <UIScrollViewDelegate, BxPageScrollTarget>{
@protected
	BxPageScrollView * _pageScroll;
    int _currentIndex;
}

//! массив элементов BxLoadedImageListItem для отображения элементов на основе BxLoadedImagePageView
@property (nonatomic, retain) NSArray * itemsData;

- (void) startWithData:(NSArray*) itemsData currentIndex:(int) index;

// protected:
- (void) updatePageScroll;
- (void) updatePageWithItem: (BxLoadedImageListItem*) item;

@end
