//
// Created by Balalaev Sergey on 9/23/14.
// Copyright (c) 2014 Byterix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSIndexPath (CustomDocumentTableView)

- (NSIndexPath*) jointIndexPath: (NSIndexPath*) indexPath;

- (BOOL) isEqualToIndexPath: (NSIndexPath*) indexPath;

- (NSComparisonResult)compareWithoutLevel:(NSIndexPath *)otherObject;

@end