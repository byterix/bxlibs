//
//  BxStandartRateView.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 9/10/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BxStandartRateView;

@protocol BxStandartRateViewDelegate

- (void) standartRateView: (BxStandartRateView *) rateView ratingDidChange: (float) rating;

@end

@interface BxStandartRateView : UIView

@property (nonatomic, retain) UIImage *notSelectedImage;
@property (nonatomic, retain) UIImage *halfSelectedImage;
@property (nonatomic, retain) UIImage *selectedImage;
@property (nonatomic, assign) float rating;
@property (assign) BOOL editable;
@property (assign) BOOL rounded;
@property (nonatomic, assign) int maxRating;
@property (nonatomic, assign) int midMargin;
@property (nonatomic, assign) int leftMargin;
@property (nonatomic, assign) CGSize minImageSize;
@property (nonatomic, assign) id <BxStandartRateViewDelegate> delegate;

@end
