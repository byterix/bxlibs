/**
 *	@file DialogController.h
 *	Нижняя панель с кнопками, в диалоговом режиме
 *	@date 01.11.2012
 *  @author Sergey Balalaev
 *	@warning Copyright 2012, Byterix. See http://byterix.net
 */

#import <Foundation/Foundation.h>
#import "BxServiceController.h"

@interface BxDialogController : BxServiceController {
@protected
	UIView * contentView;
	UILabel * titleLabel;
    float titleHeight;
    UIImageView * backMessagePanel;
}

@end
