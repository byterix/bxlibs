//
//  BxPageScrollView.h
//  iBXVcl
//
//  Created by Sergan on 09.10.13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BxPageView.h"

@class BxPageScrollView;

@protocol BxPageScrollTarget

- (void) pageScrollView: (BxPageScrollView*) pageScrollView updatePage: (BxPageView*) page fromIndex: (int) index;
- (void) pageScrollView: (BxPageScrollView*) pageScrollView showPage:(BxPageView*) page fromIndex: (int) index;

@end

@interface BxPageScrollView : UIScrollView <UIScrollViewDelegate> {
@protected
	int _currentPageIndex;
	int _centerPageIndex;
	NSMutableArray * _pages;
	int _currentPageCount;
	int _cashPageCount;
}

//! количество страниц, которые гарантированно будут находится в кеше
@property (nonatomic) int bufferPageCount;
//! отступ относительно краев для страниц
@property (nonatomic) float pageIndent;
//!
@property (nonatomic, assign) id<BxPageScrollTarget> target;

- (id) initWithFrame:(CGRect) rect target:(id<BxPageScrollTarget>) target;

- (void) startWithPageCount:(int) pageCount currentIndex:(int) index classPage: (Class) classPage;

@end
