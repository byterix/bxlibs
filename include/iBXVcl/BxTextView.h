//
//  BxTextView.h
//  iBXVcl
//
//  Created by Sergan on 22/08/15.
//  Copyright (c) 2015 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BxTextView : UITextView{
@protected
    BOOL _shouldDrawPlaceholder;
}
@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;


@end
