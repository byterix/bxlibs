//
//  UIViewController+BxVcl.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 11/25/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BxVcl)

//! Для любой версии iOS вернет края области, которая показывается на экране без наложенных на них панелей
- (CGRect) extendedEdgesBounds;

//! Для любой версии iOS вернет края области, которая показывается на экране без наложенных на них панелей
- (CGFloat) topExtendedEdges;
- (CGFloat) bottomExtendedEdges;

//! возвращает показанный на экране контроллер
+ (UIViewController *) topMostController;

@end
